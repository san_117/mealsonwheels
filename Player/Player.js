class Player extends GameObject
{
    constructor(x,y,z,scene)
    {
        super();
        this.x = x;
        this.y = y;
        this.z = z;
        this.carMesh = undefined;
        this.buildPlayer(this.x*(size+offset),this.y,this.z*(size+offset),0,scene.scene,12);
        this.dir;
    }

    start(){

    }

    update(){

    }

    onDestroy(){

    }

    UpdatePosition(dir){
        //this.rotatePlayerY(dir); 
        this.carMesh.position.set(this.x*16,this.y,this.z*16);     
    }

    moveMesh(timeX,timeZ,dir)
    {        
        this.rotatePlayerY(dir);
        this.carMesh.position.set((this.x+timeX)*16,this.y,(this.z+timeZ)*16);
    }

    buildPlayer(x,y,z,rotation,scene,size){
        var extra = getResource("Player");    
        extra.scale.set(size,size,size);
        extra.rotation.set(0,(Math.PI/180) * rotation,0);
        extra.position.set(x,y,z);
        this.carMesh = extra;

        scene.add(this.carMesh);         
    }

    rotatePlayerY(dir){
        if(dir != this.dir){
            switch(dir)
            {
                case 0:this.carMesh.rotation.set(0,Math.PI,0);break;
                case 1:this.carMesh.rotation.set(0,Math.PI/2,0);break;
                case 2:this.carMesh.rotation.set(0,Math.PI,0);break;
                case 3:this.carMesh.rotation.set(0,Math.PI/2,0);break;
                default:break;
            }
            this.dir = dir;
        }
    }
}