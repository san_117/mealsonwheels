var keys; // only for listener

document.addEventListener('keydown', function(event) 
{
    keys = event.keyCode;
    //console.log(keys)
});

class PlayerController extends GameObject
{
    constructor(x,y,z,scene,world)
    {
        super();
        this.player = new Player(x,y,z,scene);
        this.world = world;
        this.street_Tile = this.world.grid[x][z];
        this.actualDirection = directions.RIGHT;
        this.timer = 0;
        this.moveWithDirection = false;
        this.scene = scene;
        this.updateSpeed = 0.3;
        //this.minimap = scene.minimap.map;

		this.debuguers = [];
    }

    start(){

    }

    update()
    {        
        if(lives==0)
            return;

        this.timer+=deltaTime;
        if(keys == 40){
            if(this.street_Tile.checkDirection(directions.UP) && this.actualDirection != directions.DOWN){
				this.actualDirection = directions.UP;
			}
        }
        if(keys == 39){
            if(this.street_Tile.checkDirection(directions.RIGHT)&& this.actualDirection != directions.LEFT){
				this.actualDirection = directions.RIGHT;
			}
        }
        if(keys == 38){
            if(this.street_Tile.checkDirection(directions.DOWN) && this.actualDirection != directions.UP){
				this.actualDirection = directions.DOWN;
			}
        }
        if(keys ==37){
            if(this.street_Tile.checkDirection(directions.LEFT) && this.actualDirection != directions.RIGHT){
				this.actualDirection = directions.LEFT;
			}
        }
        this.smoothMove(this.actualDirection); 
        if(this.timer>=this.updateSpeed)
        {       
            this.move(this.actualDirection);
            this.timer = 0;	
        }
    }

    move(directions)
    {
        var legal_moves = this.street_Tile.getValidDirections(directions);
		
	   if(!legal_moves.includes(directions)){
			directions = legal_moves[Math.floor(Math.random() * legal_moves.length)];
            
            this.actualDirection = directions;
        }else{
			if(legal_moves.length <= 1){
				directions = legal_moves[0];
				this.actualDirection = directions;
			}
		}    
        switch(directions){

            case 2: 
                this.player.z--;      
                this.world.move(2);
                //this.minimap.follow(this.player);
                //this.player.UpdatePosition(0);
                break;

            case 3: 
                this.player.x--;
                this.world.move(3);
                //this.minimap.follow(this.player);
                //this.player.UpdatePosition(1);
                break;

            case 1: 
                this.player.x++;
                this.world.move(1);
                //this.minimap.follow(this.player);
                //this.player.UpdatePosition(1);
            break;

            case 0: 
                this.player.z++;

                this.world.move(0);
                //this.minimap.follow(this.player);
               // this.player.UpdatePosition(0);
            break;

            default: break;
        }
        this.street_Tile = this.world.grid[this.player.x][this.player.z];
    }

    smoothMove(directions)
    {
        switch(directions){

            case 2: 
            this.world.scene.camera.position.z=((this.player.z-(this.timer/this.updateSpeed))*16)+100;
            this.player.moveMesh(0,-this.timer/this.updateSpeed,directions);
            break;

            case 3: 
            this.world.scene.camera.position.x=((this.player.x-(this.timer/this.updateSpeed))*16)+100;
            this.player.moveMesh(-this.timer/this.updateSpeed,0,directions);
            break;

            case 1: 
            this.world.scene.camera.position.x=((this.player.x+(this.timer/this.updateSpeed))*16)+100;
            this.player.moveMesh(this.timer/this.updateSpeed,0,directions);
            break;

            case 0: 
            this.world.scene.camera.position.z=((this.player.z+(this.timer/this.updateSpeed))*16)+100;
            this.player.moveMesh(0,this.timer/this.updateSpeed,directions);
            break;

            default: break;
        }
    }
}