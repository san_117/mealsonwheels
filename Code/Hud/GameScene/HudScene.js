class HudScene extends Scene
{
	constructor(engine)
	{
		var camera = new THREE.OrthographicCamera(-window.innerWidth/2, window.innerWidth/2,  window.innerHeight/2, - window.innerHeight/2, 0, 30 );
		super(engine, camera);
		//active_scenes.push(this);
		this.hud = new Hud(camera,this);
		this.instantiate(this.hud);
	}
}