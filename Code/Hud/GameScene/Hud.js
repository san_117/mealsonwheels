class Hud extends GameObject
{
	constructor(camera,scene)
	{
		super();
		this.hudCanvas = document.createElement('canvas');
		this.hudCanvas.width = window.innerWidth;
		this.hudCanvas.height = window.innerHeight;
		this.utilX = window.innerWidth/20;
		this.utily = window.innerHeight/1.85;
		this.hudBitMap = this.hudCanvas.getContext('2d');
		this.score = 0;
		this.cameraHud = camera;
		this.hudTexture = new THREE.Texture(this.hudCanvas) 
		this.hudTexture.magFilter = THREE.NearestFilter;
		this.hudTexture.minFilter = THREE.NearestFilter;
		this.hudTexture.needsUpdate = true;
	  // Create HUD material.
		this.material = new THREE.MeshBasicMaterial( {map: this.hudTexture} );
		this.material.transparent = true;
		this.isGameOver = false;
		this.planeGeometry = new THREE.PlaneGeometry( window.innerWidth, window.innerHeight );
		this.plane = new THREE.Mesh( this.planeGeometry, this.material );
		this.scene = scene;
		this.phone = new Phone("../Assets/Hud/phone.png",this.utilX,this.utily,192,360);
		//this.hudBitMap.fillStyle = this.phone.background.color;
		//this.hudBitMap.fillRect(this.phone.background.x, this.phone.background.y, this.phone.background.width, this.phone.background.height);
		//this.hudBitMap.fillStyle = "rgba(0,0,0,1)";
		this.scene.scene.add(this.plane);
		return;
		this.stats = new Stats();
		this.stats.domElement.style.position = 'absolute';
		this.stats.domElement.style.top = '100px';
		this.stats.domElement.style.left = '15px';
		document.body.appendChild( this.stats.domElement );
		
	}
	
	start()
	{
		
	}
	
	update()
	{
		
		this.hudBitMap.clearRect(0, 0, window.innerWidth, innerHeight);
		if(!this.isGameOver)
		{
			this.hudBitMap.imageSmoothingEnabled = false;
			this.hudBitMap.fillStyle = this.phone.background.color;
			this.hudBitMap.fillRect(this.phone.background.x, this.phone.background.y, this.phone.background.width, this.phone.background.height);
			this.hudBitMap.fillStyle = "rgba(0,0,0,1)";	
			this.hudBitMap.font = "20px Arial";
			this.hudBitMap.drawImage(this.phone.sprite, this.phone.x, this.phone.y, this.phone.width, this.phone.height);
			this.hudBitMap.fillText("Score: " + this.score, this.phone.x+60, this.phone.y+141);
			this.phone.update(this.hudBitMap);
		}
		else
		{
			this.gameOver();
		}
		this.hudTexture.needsUpdate = true;
		//this.stats.update();
	}
	
	gameOver()
	{
		console.log("GAMEOVER");
		this.isGameOver = true;
		var imgBack = new Image();
		imgBack.src = "../Assets/Hud/Icons/main menu.png";
		var gameOverYeah = new Image()
		gameOverYeah.src = "../Assets/Hud/gameover.png"
		this.hudBitMap.fillStyle = "rgba(30,104,255,1)";
		this.hudBitMap.font = "30px Arial";
		this.hudBitMap.fillRect(window.innerWidth/3,window.innerHeight/3,400,300);
		this.hudBitMap.fillStyle = "rgba(0,0,0,1)"
		this.hudBitMap.drawImage(gameOverYeah,window.innerWidth/2.75,window.innerHeight/3,300,180)
		this.hudBitMap.fillText("Score: "+ this.score,window.innerWidth/2.6,window.innerHeight/1.7)
		this.hudBitMap.drawImage(imgBack,window.innerWidth/2.5,window.innerHeight/1.6,180,60);
		this.hudTexture.needsUpdate = true;
	}
	
	updateScore(score)
	{
		this.score = score;
	}
	
}