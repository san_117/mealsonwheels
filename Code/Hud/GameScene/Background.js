class Background
{
	constructor(x,y,width,height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.colors = [];
		var green = "#00FF00";
		var yellow = "#fff200";
		var red = "#ff0000";
		this.colors.push(green,yellow,red);
		this.color = this.colors[0];
	}
	
	update()
	{
		if(this.width >= 0)
		{
			this.width -= deltaTime * 0.5;
		}
		this.changeColor();
	}
	
	changeColor()
	{
		if(this.width >= 70)
		{
			this.color = this.colors[0];
		}
		else if (this.width >= 30 && this.width < 70)
		{
			this.color = this.colors[1];
		}
		else if(this.width < 30)
		{
			this.color = this.colors[2];
		}
	}
	
	setColor(i)
	{
		this.color =this.colors[i];
	}
}