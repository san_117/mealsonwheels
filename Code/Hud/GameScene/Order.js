class Order
{
	constructor(x,y, foodWidth, foodHeight, barWidth, barHeight, timeBar, foodImage, houseImage, game_manager_order)
	{
		this.x = x;
		this.y = y;
		this.timeBar = timeBar;
		this.foodImage = foodImage;
		this.foodWidth = foodWidth;
		this.foodHeight = foodHeight;
		this.isActive = true;
		this.bar = new Background(x+foodWidth+1,y,barWidth,barHeight);
		this.game_order = game_manager_order;
	}
	
	update(position,y)
	{
		this.y = y +(150+(position*51));
		this.bar.y = this.y;
	}
}