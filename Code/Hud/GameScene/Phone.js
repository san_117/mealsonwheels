class Phone
{
	constructor(imagesrc,x,y,width,height)
	{
		this.sprite = new Image();
		this.sprite.src = imagesrc;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.messeges = [];
		this.background = new Background("#FFA500",this.x+5,this.y+5,this.width-5,this.height-5);
		this.time = 100;
		this.currentTime = 100;
		this.speedUp = 0;
		this.icons = []
		this.house_icons = []
		
		this.timeBar = new Image();
		this.timeBar.src = "../Assets/Hud/timeBar.png";
		
		//kebab
		var arab1 = new Image();
		arab1.src = "../Assets/Hud/Food/Arab00.png";
		//pizza
		var itialian1 = new Image();
		itialian1.src = "../Assets/Hud/Food/Italian00.png";
		//sushi
		var japan1 = new Image();
		japan1.src = "../Assets/Hud/Food/Japan00.png";
		//hamburguer
		var urban1 = new Image();
		urban1.src = "../Assets/Hud/Food/Urban00.png";
		//taco
		var mexican1 = new Image();
		mexican1.src = "../Assets/Hud/Food/Mexican00.png";

		this.icons.push(arab1);
		this.icons.push(itialian1);
		this.icons.push(japan1);
		this.icons.push(urban1);
		this.icons.push(mexican1);
	
		//var type = (Math.trunc(Math.random() * 8) + 1);
		//var order = new Order(this.x+15,this.y+150,50,50,100,50,this.Images[0],type,this.Images[type]);
		var dummy = new Order(this.x+15,this.y+150,50,50,100,50,this.icons[0],0,this.icons[0]);
		dummy.isActive = false;
		//this.messeges.push(order);
		this.messeges.push(dummy,dummy,dummy);
	}
	
	update(hudBitMap)
	{
	//	if(this.currentTime < this.time){
	//		this.currentTime += deltaTime;
	//		return;
	//	}
	//	this.currentTime = 0;
		var count = 0;

		for(var i = 0; i<this.messeges.length; i++)
		{
			if(this.messeges[i].isActive)
			{
				hudBitMap.fillStyle = this.messeges[i].bar.color;
				this.messeges[i].bar.update();
				
				hudBitMap.fillRect(this.messeges[i].bar.x, this.messeges[i].bar.y, this.messeges[i].bar.width, this.messeges[i].bar.height);
				hudBitMap.fillStyle = "rgba(0,0,0,1)";	
				hudBitMap.drawImage(this.messeges[i].foodImage, this.messeges[i].x, this.messeges[i].y, this.messeges[i].foodWidth, this.messeges[i].foodHeight);
				hudBitMap.drawImage(this.messeges[i].timeBar, this.messeges[i].bar.x, this.messeges[i].bar.y, this.messeges[i].barWidth, this.messeges[i].barHeight);
				this.orderTimeOut(this.messeges[i],i);
				count++;
			}
		}
	}

	createOrder(food_theme, house_theme, game_manager_order)
	{
		var position = this.checkNonActiveSpot();
		var order = new Order(this.x+15,this.y+(150+(position*51)),50,50,100,50,this.timeBar,this.icons[food_theme], this.house_icons[house_theme], game_manager_order);
		this.messeges[position] = order;
	}
	
	orderTimeOut(order,i)
	{
		
		if(order.bar.width <= 0)
		{
			this.messeges[i].isActive = false;
			this.messeges[i].game_order.failOrder();
		
			for(var j = i; j< this.messeges.length;j++)
			{
				if(j == 0 || j==1)
				{
					var help = this.messeges[j];
					this.messeges[j] = this.messeges[j+1];
					this.messeges[j+1] = help;
				}
				this.messeges[j].update(j,this.y);
			}
		}
	}
	
	checkNonActiveSpot()
	{
		for(var i = 0; i < this.messeges.length;i++)
		{
			if(!this.messeges[i].isActive)
			{
				return i;
			}
		}

		return 0;
	}
	
	
	
}
