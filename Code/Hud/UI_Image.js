class UI_Image{
	constructor(x,y,width,height,texture,scene){
		var canvas = document.createElement('canvas');
		var context = canvas.getContext('2d');
		texture.magFilter = THREE.NearestFilter;
		texture.minFilter = THREE.NearestFilter;
		var material = new THREE.MeshBasicMaterial({map: texture});
		material.transparent = true;
		this.mesh = new THREE.Mesh(new THREE.PlaneGeometry(width, height), material);
		this.mesh.position.set(x,y,0);
		this.mesh.needsUpdate = true;
		this.mesh.renderOrder = 10;
		scene.add(this.mesh);
	}

	move(x,y){
		this.mesh.position.set(x,y,0);
	}
}