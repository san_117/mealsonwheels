class Toggle
{
	constructor(imgTrue, imgFalse, x,y,width,height)
	{
		this.imgTrue = new Image();
		this.imgTrue.src = imgTrue;
		this.imgFalse = new Image();
		this.imgFalse.src = imgFalse;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
}