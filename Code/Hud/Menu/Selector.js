class Selector
{
	constructor(imagesrc,x,y,width,height)
	{
		this.sprite = new Image();
		this.sprite.src = imagesrc;
		this.x = x;
		this.y = y;
		this.originalY = y;
		this.originalX = x;
		this.width = width;
		this.height = height;
		this.position = 0;
	}
	update(i,isBack)
	{
		this.x = this.originalX;
		this.y = this.originalY + (i*this.position);
		
		if(this.position == 3 && isBack)
		{
			this.x = window.innerWidth/2;
			this.y = window.innerHeight/1.5;
		}
	}
}