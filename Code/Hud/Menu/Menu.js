class Menu extends GameObject
{
	constructor(camera,scene,activeScene)
	{
		super();
		this.hudCanvas = document.createElement('canvas');
		this.hudCanvas.width = window.innerWidth;
		this.hudCanvas.height = window.innerHeight;
		this.utilX = window.innerWidth/2.41;
		this.utily = window.innerHeight/1.7;
		this.hudBitMap = this.hudCanvas.getContext('2d');
		
		this.cameraHud = camera;
		this.hudTexture = new THREE.Texture(this.hudCanvas) 
		this.hudTexture.needsUpdate = true;
	  // Create HUD material.
		this.material = new THREE.MeshBasicMaterial( {map: this.hudTexture} );
		this.material.transparent = true;
		
		this.planeGeometry = new THREE.PlaneGeometry( window.innerWidth, window.innerHeight );
		this.plane = new THREE.Mesh( this.planeGeometry, this.material );
		this.scene = scene;
		
		this.menuImage = new Image();
		this.menuImage.src = "../Assets/Hud/menu.png";
		this.selector = new Selector("../Assets/Hud/menu_selector.png", this.utilX,this.utily, 250,100);
		this.optionsSelector =  new Selector("../Assets/Hud/menu_selector2.png", window.innerWidth/3.2,window.innerHeight/2.5, 25,50);
		this.difCheck1 = new Toggle("../Assets/Hud/Icons/toggle_true.png","../Assets/Hud/Icons/toggle_false.png",window.innerWidth/3,window.innerHeight/2.5,50,50);
		this.difCheck2 = new Toggle("../Assets/Hud/Icons/toggle_true.png","../Assets/Hud/Icons/toggle_false.png",window.innerWidth/2.727,window.innerHeight/2.5,50,50);
		this.difCheck3 = new Toggle("../Assets/Hud/Icons/toggle_true.png","../Assets/Hud/Icons/toggle_false.png",window.innerWidth/2.5,window.innerHeight/2.5,50,50);
		this.musicCheck = new Toggle("../Assets/Hud/Icons/music_on.png","../Assets/Hud/Icons/music_off.png",window.innerWidth/3,window.innerHeight/2.01,50,50);
		this.soundCheck = new Toggle("../Assets/Hud/Icons/sound_on.png","../Assets/Hud/Icons/sound_off.png",window.innerWidth/3,window.innerHeight/1.7,50,50);
		this.backCheck = new Image();
		this.backCheck.src = "../Assets/Hud/Icons/ready.png";
		this.background1 = new Image();
		this.background1.src = "../Assets/Hud/menu_options.png";
		this.background2 = new Background(0,0,window.innerWidth,window.innerHeight);
		this.background2.setColor(1);
		this.scene.scene.add(this.plane);
		this.menuScene = 0;
		this.dificulty = 1;
		this.isMusicMute = false;
		this.isSFXMute = false;
		
		
		this.activeScene = activeScene
		
	}
	
	start()
	{
		
	}
	
	update()
	{
		input(this);
		this.render();
	}
	
	render()
	{
	console.log("menu");
		this.hudBitMap.clearRect(0, 0, window.innerWidth, innerHeight);
		this.hudBitMap.imageSmoothingEnabled = false;
		this.hudBitMap.fillStyle = "rgba(0,0,0,1)";
		
		if(this.menuScene == 0)
		{
			this.hudBitMap.drawImage(this.menuImage,0,0,window.innerWidth,window.innerHeight);
			this.hudBitMap.drawImage(this.selector.sprite,this.selector.x,this.selector.y,this.selector.width,this.selector.height);
		}
		else if(this.menuScene == 1)
		{
			
			this.hudBitMap.font = "30px Arial";
			this.hudBitMap.drawImage(this.background1,0,0,window.innerWidth,window.innerHeight);
			console.log(this.difCheck1.imgTrue);
			this.hudBitMap.drawImage(this.difCheck1.imgTrue,this.difCheck1.x,this.difCheck1.y,this.difCheck1.width,this.difCheck1.height);
			if(this.dificulty >= 2)
			{
				this.hudBitMap.drawImage(this.difCheck2.imgTrue,this.difCheck2.x,this.difCheck2.y,this.difCheck2.width,this.difCheck2.height);
			}
			else
			{
				this.hudBitMap.drawImage(this.difCheck2.imgFalse,this.difCheck2.x,this.difCheck2.y,this.difCheck2.width,this.difCheck2.height);
			}
			
			if(this.dificulty == 3)
			{
				this.hudBitMap.drawImage(this.difCheck3.imgTrue,this.difCheck3.x,this.difCheck3.y,this.difCheck3.width,this.difCheck3.height);
			}
			else
			{
				this.hudBitMap.drawImage(this.difCheck3.imgFalse,this.difCheck3.x,this.difCheck3.y,this.difCheck3.width,this.difCheck3.height);
			}
			
			this.hudBitMap.fillText("Music", this.musicCheck.x + this.musicCheck.width+10, this.musicCheck.y + this.musicCheck.height-10);
			if(!this.isMusicMute)
			{
				this.hudBitMap.drawImage(this.musicCheck.imgTrue,this.musicCheck.x,this.musicCheck.y,this.musicCheck.width,this.musicCheck.height);
			}
			else
			{
				this.hudBitMap.drawImage(this.musicCheck.imgFalse,this.musicCheck.x,this.musicCheck.y,this.musicCheck.width,this.musicCheck.height);
			}
			
			this.hudBitMap.fillText("Sound Effects", this.soundCheck.x + this.soundCheck.width+10, this.soundCheck.y + this.soundCheck.height-10);
			if(!this.isSFXMute)
			{
				this.hudBitMap.drawImage(this.soundCheck.imgTrue,this.soundCheck.x,this.soundCheck.y,this.soundCheck.width,this.soundCheck.height);
			}
			else
			{
				this.hudBitMap.drawImage(this.soundCheck.imgFalse,this.soundCheck.x,this.soundCheck.y,this.soundCheck.width,this.soundCheck.height);
			}
			
			if(this.dificulty == 1)
			{
				this.hudBitMap.fillText("Dificulty: easy", this.difCheck3.x + this.difCheck3.width+10, this.difCheck3.y + this.difCheck3.height-10);
			}
			else if(this.dificulty == 2)
			{
				this.hudBitMap.fillText("Dificulty: medium", this.difCheck3.x + this.difCheck3.width+10, this.difCheck3.y + this.difCheck3.height-10);
			}
			else if(this.dificulty == 3)
			{
				this.hudBitMap.fillText("Dificulty: hard", this.difCheck3.x + this.difCheck3.width+10, this.difCheck3.y + this.difCheck3.height-10);
			}
			
			this.hudBitMap.drawImage(this.backCheck,window.innerWidth/1.9,window.innerHeight/1.5,180,60);
			this.hudBitMap.drawImage(this.optionsSelector.sprite,this.optionsSelector.x,this.optionsSelector.y,this.optionsSelector.width,this.optionsSelector.height);
		}
		else if(this.menuScene == 2)
		{
			this.hudBitMap.fillStyle = this.background2.color;
			this.hudBitMap.fillRect(this.background2.x,this.background2.y,this.background2.width,this.background2.height);
			this.hudBitMap.fillStyle = "rgba(0,0,0,1)";
			this.hudBitMap.drawImage(this.backCheck,window.innerWidth/1.9,window.innerHeight/1.5,180,60);
		}
		this.hudTexture.needsUpdate = true;
	}
	
	onKeyDown(ev)
	{
		switch (ev.keyCode)
		{
			case 83: // s
				console.log("s");
				if(this.menuScene == 0)
				{
					if(this.selector.position < 3)
					{
						this.selector.position++;
					}
				}
				else if(this.menuScene == 1)
				{
					if(this.optionsSelector.position<3)
					{
						this.optionsSelector.position++;
					}
				}
				break;
			case 87: // w
				console.log("w");
				if(this.menuScene == 0)
				{
					if(this.selector.position > 0)
					{
						this.selector.position--;
					}
				}
				else if(this.menuScene == 1)
				{
					if(this.optionsSelector.position > 0)
					{
						this.optionsSelector.position--;
					}
				}
				break;
			case 65: // a
				if(this.menuScene == 1)
				{
					if(this.optionsSelector.position == 0)
					{
						if(this.dificulty > 1)
						{
							this.dificulty--;
						}
					}
				}
				break;
			case 68: // d
				if(this.menuScene == 1)
				{
					if(this.optionsSelector.position == 0)
					{
						if(this.dificulty < 3)
						{
							this.dificulty++;
						}
					}
				}
				break;
			case 37: // left arrow
				if(this.menuScene == 1)
				{
					if(this.optionsSelector.position == 0)
					{
						if(this.dificulty > 1)
						{
							this.dificulty--;
						}
					}
				}
				break;
			case 39: // right arrow
				if(this.menuScene == 1)
				{
					if(this.optionsSelector.position == 0)
					{
						if(this.dificulty < 3)
						{
							this.dificulty++;
						}
					}
				}
				break;
			case 38: // up arrow
				console.log("up")
				if(this.menuScene == 0)
				{
					if(this.selector.position > 0)
					{
						this.selector.position--;
					}
				}
				else if(this.menuScene == 1)
				{
					if(this.optionsSelector.position > 0)
					{
						this.optionsSelector.position--;
					}
				}
				break;
			case 40: //down arrow
				console.log("down");
				if(this.menuScene == 0)
				{
					if(this.selector.position < 3)
					{
						this.selector.position++;
					}
				}
				else if(this.menuScene == 1)
				{
					if(this.optionsSelector.position<3)
					{
						this.optionsSelector.position++;
					}
				}
				break;
			case 13: //enter
				console.log("eneter");
				if(this.menuScene == 0)
				{
					if(this.selector.position == 0)
					{
						var world_scene = new WorldScene(this);
						active_scenes.push(world_scene);
						active_scenes.push(world_scene.hud);
						this.scene.unLoad();
					}
					else if(this.selector.position == 1)
					{
						this.menuScene = 1;
					}
					else if(this.selector.position == 2)
					{
						this.menuScene = 2;
					}
				}
				else if(this.menuScene == 1)
				{
					if(this.optionsSelector.position == 1)
					{
						if(this.isMusicMute)
						{
							this.isMusicMute = false;
						}
						else
						{
							this.isMusicMute = true;
						}
					}
					else if(this.optionsSelector.position == 2)
					{
						if(this.isSFXMute)
						{
							this.isSFXMute = false;
						}
						else
						{
							this.isSFXMute = true;
						}
					}
					else if(this.optionsSelector.position == 3)
					{
						this.menuScene = 0;
					}
				}
				else
				{
					this.menuScene = 0;
				}
				break;
			default: break;
		}
		if(this.menuScene == 0)
		{
			this.selector.update(40,false);
		}
		else
		{
			this.optionsSelector.update(71,true);
		}
		this.render();
		
	}
	
}

function input(menu)
{
	document.onkeydown = function(ev)
	{ 
		menu.onKeyDown(ev); 
	};
}