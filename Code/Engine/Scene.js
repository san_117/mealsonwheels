var next_hash_code = 0;

class Scene{
	constructor(engine, cam){
		this.scene = new THREE.Scene();
		//this.scene.background = new THREE.Color( 0xffffff );
		this.camera = cam;
		this.objects = {};
		this.unload = false;
	}

	updateObjectsInScene(){
		for(var key in this.objects){
			var value = this.objects[key];
			value.update();
		}
	}

	getCamera(){
		return this.camera;
	}

	getScene(){
		return this.scene;
	}

	unLoad(){
		this.unload = true;
	}

	instantiate(gameObject){
		if(!this.objects.hasOwnProperty(gameObject.hashCode)){

			gameObject.hashCode = next_hash_code;
			this.objects[next_hash_code] = gameObject;
			next_hash_code++;

			gameObject.start();
		}
	}

	destroy(gameObject){
		
		if(this.objects.hasOwnProperty(gameObject.hashCode)){
			this.scene.rem( gameObject );
			gameObject.onDestroy();

			delete this.objects[gameObject.hashCode];
		}
	}
}