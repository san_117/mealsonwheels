var resources = {};

var urban_buildings_houses = [];
var urban_buildings_houses_small = [];
var urban_buildings_food = [];
var urban_buildings_deco = [];

var arab_buildings_houses = [];
var arab_buildings_houses_small = [];
var arab_buildings_food = [];
var arab_buildings_deco = [];

var japan_buildings_houses = [];
var japan_buildings_houses_small = [];
var japan_buildings_food = [];
var japan_buildings_deco = [];

var mexican_buildings_houses = [];
var mexican_buildings_houses_small = [];
var mexican_buildings_food = [];
var mexican_buildings_deco = [];

var italian_buildings_houses = [];
var italian_buildings_houses_small = [];
var italian_buildings_food = [];
var italian_buildings_deco = [];

function getResource(url){
	return resources[url].clone();
}

var loader = new THREE.OBJLoader();

function startLoad(loader){
	loader.load('../Assets/Vox/Streets/street_I.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {

				resources["Street_I"] = child; 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Streets/street_L.obj', function onLoad (object) {
	object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {

				resources["Street_L"] = child; 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Streets/street_T.obj', function onLoad (object) {
	object.traverse( function ( child ) 
	{
		if ( child.isMesh ) {

			resources["Street_T"] = child; 
			
			child.material = new THREE.MeshStandardMaterial();
			var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
			texture.encoding = THREE.sRGBEncoding;
			texture.anisotropy = 16;

			child.material.map = texture;
			child.flatShading = true;
			child.castShadow = true;
		}
	} );
});


	loader.load('../Assets/Vox/Streets/street_X.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {

				resources["Street_X"] = child; 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/debug.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {

				resources["Debug"] = child; 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/arrow.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {

				resources["Arrow"] = child; 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/debug2x2.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {

				resources["Debug2X2"] = child; 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Urban/food_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				urban_buildings_food.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Urban/food_1x1_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				urban_buildings_food.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Urban/deco_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				urban_buildings_deco.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Urban/house_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				urban_buildings_houses_small.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Urban/deco_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				urban_buildings_houses_small.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Urban/deco_1x1_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				urban_buildings_deco.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Urban/house_2x2_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				urban_buildings_houses.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Urban/house_2x2_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				urban_buildings_houses.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Urban/house_2x2_02.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				urban_buildings_houses.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Car/car0.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {

				resources["Player"] = child; 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Arab/food_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				arab_buildings_food.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Arab/food_1x1_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				arab_buildings_food.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Arab/deco_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				arab_buildings_deco.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Arab/house_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				arab_buildings_houses_small.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Arab/house_1x1_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				arab_buildings_houses_small.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Arab/house_1x1_02.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				arab_buildings_houses_small.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Arab/house_2x2_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				arab_buildings_houses.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Arab/house_2x2_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				arab_buildings_houses.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Japan/deco_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				japan_buildings_deco.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Japan/deco_1x1_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				japan_buildings_deco.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Japan/deco_1x1_02.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				japan_buildings_deco.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Japan/food_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				japan_buildings_food.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Japan/food_1x1_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				japan_buildings_food.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Japan/house_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				japan_buildings_houses_small.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Japan/house_2x2_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				japan_buildings_houses.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Japan/house_2x2_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				japan_buildings_houses.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Mexican/house_2x2_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				mexican_buildings_houses.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Mexican/house_2x2_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				mexican_buildings_houses.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Mexican/house_2x2_02.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				mexican_buildings_houses.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Mexican/house_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				mexican_buildings_houses_small.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Mexican/house_1x1_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				mexican_buildings_houses_small.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Mexican/house_1x1_02.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				mexican_buildings_houses_small.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Mexican/house_1x1_03.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				mexican_buildings_houses_small.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Mexican/food_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				mexican_buildings_food.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Mexican/food_1x1_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				mexican_buildings_food.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Mexican/food_1x1_02.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				mexican_buildings_food.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Mexican/deco_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				mexican_buildings_deco.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Mexican/deco_1x1_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				mexican_buildings_deco.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Italian/deco_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				italian_buildings_deco.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Italian/house_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				italian_buildings_houses_small.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Italian/house_2x2_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				italian_buildings_houses.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Italian/house_2x2_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				italian_buildings_houses.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Italian/food_1x1_00.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				italian_buildings_food.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});

	loader.load('../Assets/Vox/Italian/food_1x1_01.obj', function onLoad (object) {
		object.traverse( function ( child ) 
		{
			if ( child.isMesh ) {
	
				italian_buildings_food.push(child); 
			
				child.material = new THREE.MeshStandardMaterial();
				var texture = new THREE.TextureLoader().load( '../Assets/Textures/tile.png' );
				texture.encoding = THREE.sRGBEncoding;
				texture.anisotropy = 16;

				child.material.map = texture;
				child.flatShading = true;
				child.castShadow = true;
			}
		} );
	});
}


