var running;
var fps = 1/60;
var renderer;
var active_scenes = [];
var deltaTime = 0;
var lastUpdate = Date.now();

class GameEngine{

	constructor(){

		running = true;
		renderer = new THREE.WebGLRenderer({ antialiasing: true });
		renderer.setSize( window.innerWidth, window.innerHeight );
		//renderer.shadowMap = true;
		//renderer.shadowMap = THREE.PCFSoftShadowMap;
		//renderer.shadowMapSoft = true;
		renderer.autoClear = false;
		renderer.gammaFactor = 2.2;
		renderer.gammaOutput = true;
		document.body.appendChild( renderer.domElement );
		//var world_scene = new WorldScene(this); //Like prefab!
		var menuScene = new MenuScene(this);
		
		
		//active_scenes.push(world_scene);
		//active_scenes.push(world_scene.hud);
		//active_scenes.push(world_scene.minimap);
		active_scenes.push(menuScene);
	}

	update(){
	
		if(!running)
			return;

		var now = Date.now();
		deltaTime = (now - lastUpdate)/1000;
		lastUpdate = now;

		for(var i = 0; i < active_scenes.length; i++){
			if(active_scenes[i].unload){
				active_scenes.splice(i,1);
				return;
			}
			this.renderScene(active_scenes[i]);
		}
	}

	renderScene(scene){
		
		scene.updateObjectsInScene();
		renderer.render( scene.getScene(), scene.getCamera() );
	}

	stopEngine(){
		running = false;
	}
}

main();

var engine;

function main(){
	startLoad(new THREE.OBJLoader());

	setTimeout(function () {
		
		engine = new GameEngine();
		loop();
    }, 5000);
}


function loop(){
	engine.update();
	setTimeout(loop, fps);
}