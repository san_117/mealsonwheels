class WorldScene extends Scene{
	constructor(engine){
		var camera = new THREE.OrthographicCamera( window.innerWidth / - 2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / - 2, -1000, 1000 );
		camera.position.x = 50;
		camera.position.y = 300;
		camera.position.z = 50;
		camera.lookAt(-50,0,-50);
		camera.zoom = 3;
		camera.updateProjectionMatrix();

		super(engine,camera);

		this.scene.background = new THREE.Color('#bada55');
		this.scene.fog = new THREE.FogExp2('white', 0.002);
		this.hud = new HudScene(engine);
		
		this.world = new World(50, this);
		//this.minimap = new Minimap(engine,this.getScene());

		this.camera = camera;
		this.instantiate(this.world);


		this.player = new PlayerController(1,16,0,this,this.world);
		this.instantiate(this.player);	

		this.gamemanager = new GameManager(this.world, this.player, this.hud);
		this.instantiate(this.gamemanager);
	}
}