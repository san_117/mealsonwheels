var ticks = 0;
var lastPos;


class World extends GameObject{

	constructor(size, scene){
		super();
		this.size = size;
		this.grid = [];
		this.scene = scene;

		this.renderDistance = 7;
		this.focus_position = new THREE.Vector3(0,0,0);
		this.backgroundMusic = new Audio('../Music/Pleasant Creek Pack/Pleasant Creek.mp3');
	}

	start(){

		//this.backgroundMusic.setLoop(true);
		this.scene.camera.position.x += 50;
		this.scene.camera.position.z += 50;
		this.scene.camera.zoom +=.1;

		var shadowLight = new THREE.DirectionalLight( 0xffffff, .5 );
		shadowLight.position.set( 0, 60, 0 );
		shadowLight.castShadow = true;
		shadowLight.shadow.camera.far = 1000;
		this.scene.scene.add(shadowLight);
		
		var directionalLight = new THREE.DirectionalLight( 0xffffff, .4 );
		directionalLight.position.set( 10, 40, 50 );
		this.scene.scene.add( directionalLight );
		
		for(var i = 0;i<this.size;i++)
		{
			this.grid[i] = [];
		}

		for(var x = 0; x < this.size; x++){
			for(var y = 0; y < this.size; y++){
				this.grid[x][y] == undefined;
			}
		}

		this.createStreets();
	
		for(var x = 0; x < this.size; x++){
			for(var y = 0; y < this.size; y++){
				if(this.grid[x][y] != undefined){
					this.grid[x][y].buildRoad();
				}else{
					this.grid[x][y] = new Building_Tile(x,0,y, 0,this.scene.scene, this.grid, this.size);
				}
			}
		}

		this.createCityBlocks();

		this.focus();
		this.backgroundMusic.play();
	}

	move(dir){
		
		var i = this.focus_position.x;
		var j = this.focus_position.z;

		switch(dir){
			case 0: j++;break;
			case 1: i++;break;
			case 2: j--;break;
			case 3: i--;break;
		}

		if(!((i >= 0 && i < this.size) && (j >= 0 && j < this.size))){
			return;
		}

		for(var x = -this.renderDistance; x < this.renderDistance+1; x++){
			for(var y = -this.renderDistance; y < this.renderDistance+1; y++){
				var posX = this.focus_position.x + x;
				var posY = this.focus_position.z + y;

				if((posX >= 0 && posX < this.size) && (posY >= 0 && posY < this.size)){
					if(this.grid[posX][posY] instanceof Tile){
						this.grid[posX][posY].stopRender();
					}
				}
			}
		}
		this.focus_position.set(i,this.focus_position.y,j);

		this.focus();
	}
	
	update()
	{
	return;
		this.scene.camera.position.x = this.focus_position.x + 100;	
		this.scene.camera.position.z = this.focus_position.z + 100;	

		if(ticks > 8){
			this.move();
			ticks = 0;
		}

		if(lastPos != this.focus_position)
		{
			this.focus();			
		}

		ticks++;
	}

	focus(){
		for(var x = -this.renderDistance; x < this.renderDistance+1; x++){
			for(var y = -this.renderDistance; y < this.renderDistance+1; y++){
				var posX = this.focus_position.x + x;
				var posY = this.focus_position.z + y;

				if((posX >= 0 && posX < this.size) && (posY >= 0 && posY < this.size)){
					if(this.grid[posX][posY] instanceof Tile){
						this.grid[posX][posY].render();
					}
				}
			}
		}
		lastPos = this.focus_position;
	}

	createCityBlocks(){
		var x = Math.floor(Math.random() * this.size);
		var y = Math.floor(Math.random() * this.size);

		var closed = [];

		var squares = [];

		for(var x = 0; x < this.size; x++){
			for(var y = 0; y < this.size; y++){
				if(this.grid[x][y].constructor == Building_Tile){
					if(!closed.includes(this.grid[x][y].hashCode)){
						var square = new City_Block(this.getCityBlock(this.grid[x][y]));
						squares.push(square);
						for(var i = 0; i < square.tiles.length; i++){
							closed.push(square.tiles[i].hashCode);
						}
					}
				}		
			}
		}
	
		var districts =[];

		while(true){
			districts = [];

			for(var i = 0; i < 5; i++){
				
				var index = Math.floor(Math.random() * squares.length);
				districts.push(squares[index]);
			}

			var success = true;

			for(var e = 0; e < 5; e++){
				for(var j = 0; j < 5;j++){
					if(e != j){
						if(districts[e].center.manhattanDistanceTo(districts[j].center) <= this.size / 2){
							success = false;
							break;
						}
					}
				}
			}

			if(success)
				break;
		}

		for(var e = 0; e < 5; e++){
			districts[e].setTheme(e)
			//districts[e].debugCenter();
		}

		//Barrios satisfactoriamente repartidos

		for(var i = 0; i < squares.length; i++){
			for(var e = 0; e < 5; e++){
				if(squares[i].center == squares[e].center){
					continue;
				}
			}

			var min_dist = 100000;
			var min_index = 0;

			for(var e = 0; e < districts.length; e++){
				var dist = squares[i].center.manhattanDistanceTo(districts[e].center);
				if(dist < min_dist){
					min_dist = dist;
					min_index = e;
				}
			}

			squares[i].setTheme(min_index);
		}
	}

	createStreets(){
		
		//Generate street borders
		for(var i = 0;i<this.size;i++)
		{
			for(var j = 0; j < this.size; j++){
				if(i == 0 || i == this.size-1 || j == 0 || j == this.size-1){
				
					this.grid[i][j] = new Street_Tile(i,0,j, 0,this.scene.scene, this.grid, this.size);
				}
			}
		}
		//return;//------------------------------------------------
		//Generate horizontal avenues
		//var startY = Math.floor(Math.random() * this.size);  
		for(var y = 5; y < this.size-5; y += this.size/5){
			if(Math.floor(Math.random() * 10) > 4){
				for(var x = 1; x < this.size-1; x++){
					//this.grid[x][y] = new Street_Tile(x,0,y,0,this.scene.scene, this.grid, this.size);
					//this.grid[x][y+1] = new Avenue_Tile(x,0,y+1,0,this.scene.scene, this.grid, this.size);
					//this.scene.instantiate(this.grid[x][y]);
					//this.scene.instantiate(this.grid[x][y+1]);
				}
			}
		}
		
		//Generate vertical avenues
		//var startY = Math.floor(Math.random() * this.size);  
		for(var x = 5; x < this.size-5; x += this.size/5){
			if(Math.floor(Math.random() * 10) > 4){
				for(var y = 1; y < this.size-1; y++){
					this.grid[x][y] = new Street_Tile(x,0,y,90,this.scene.scene, this.grid, this.size);
					//this.grid[x][y] = new Avenue_Tile(x,0,y,this.scene.scene, this.grid, this.size);
					//this.grid[x+1][y] = new Avenue_Tile(x+1,0,y,this.scene.scene, this.grid, this.size);
					//this.scene.instantiate(this.grid[x][y]);
					//this.scene.instantiate(this.grid[x+1][y]);
				}
			}
		}
		
		//Generate streets
		for(var i = 0; i <1200*this.size; i++){

			var tile = undefined;
			var x;
			var y;
			
			do{
				x = Math.floor(Math.random() * this.size);
				y = Math.floor(Math.random() * this.size);

				tile = this.grid[x][y];

			}while(tile == undefined);

			//Directions:
			//NORTH		= 0
			//EAST		= 1
			//SOUTH		= 2
			//WEST		= 3
			var dir =  Math.floor(Math.random() * 4); 
			switch(dir){
				case 0: y++;break;
				case 1: x++;break;
				case 2: y--;break;
				case 3: x--;break;
			}

			var separation = 3;

			//If is inside bounds
			if((x >= 0 && x < this.size) && (y >= 0 && y < this.size))
			{
				//If the next tile is null
				if(this.grid[x][y] == undefined){
					var emptySlots = 0;

					//Count all 8 neighbors
					for(var e = -separation; e < separation+1; e++){
						for(var f = -separation; f < separation+1; f++){
							if((x+e >= 0 && x+e < this.size) && (y+f >= 0 && y+f < this.size))
								if(this.grid[x+e][y+f] != undefined)
									emptySlots++;
						}
					}
					
					//Can build
					if(emptySlots == (2*separation)+1){
						do{
							this.grid[x][y] = new Street_Tile(x,0,y,0,this.scene.scene, this.grid, this.size);

							switch(dir){
								case 0: y++;break;
								case 1: x++;break;
								case 2: y--;break;
								case 3: x--;break;
							}

							if((x >= 0 && x < this.size) && (y >= 0 && y < this.size)){
								if(this.grid[x][y] != undefined){
									break;
								}
							}else{
								break;
							}
						}while(true);
					}
				}
			}
		}
	}

	getCityBlock(root_tile){
		var closedList = [];
		var openList = [];
		var openList_tiles = [];

		var node_steps = 0;

		var result = [];
		result.push(root_tile);
		openList.push(root_tile.hashCode);
		openList_tiles.push(root_tile);

		while(openList_tiles.length > 0){
			var node = openList_tiles.pop();
			openList.pop();

			for(var dir = 0; dir < 4; dir++){
				var x = node.x;
				var y = node.z;

				switch(dir){
					case 0: y++;break;
					case 1: x++;break;
					case 2: y--;break;
					case 3: x--;break;
				}

				if(x >= 0 && x < this.size && y >= 0 && y < this.size){
				
					var child = this.grid[x][y];
					
					if(child.constructor == Building_Tile){
					
						if(closedList.includes(child.hashCode)){
							continue;
						}
					
						if(!openList.includes(child.hashCode)){
							openList.push(child.hashCode);
							openList_tiles.push(child);
							
							result.push(child);
						}

						closedList.push(node.hashCode);
					}
				}
			}

			node_steps++;

			if(node_steps >= 5000){
				console.log("max node reach");
				break;
			}
		}

		return result;
	}
}