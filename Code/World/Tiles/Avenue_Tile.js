﻿class Avenue_Tile extends Street_Tile{
	constructor(x,y,z,rotation,scene, grid, gridSize){
		super(x,y,z,rotation,scene, grid, gridSize);
	}

	buildRoad(){
		this.updateNeighbors(this.x, this.z);
		return;
		var code = "";

		if(this.neighbors[directions.UP] != undefined)
			code += "1";
		else
			code += "0";

		if(this.neighbors[directions.LEFT] != undefined)
			code += "1";
		else
			code += "0";

		if(this.neighbors[directions.DOWN] != undefined)
			code += "1";
		else
			code += "0";

		if(this.neighbors[directions.RIGHT] != undefined)
			code += "1";
		else
			code += "0";

		switch(code){
			case "1010": //║
				this.buildTile("Street_I", this.x, this.y, this.z, 0, this.scene);
				return;
			case "0101": //═
				this.buildTile("Street_I", this.x, this.y, this.z, 90, this.scene);
				return;
			case "0110": //╔
				this.buildTile("Street_L", this.x, this.y, this.z, 0, this.scene);
				return;
			case "0011": //╗
				this.buildTile("Street_L", this.x, this.y, this.z, 90, this.scene);
				return;
			case "1001": //╝
				this.buildTile("Street_L", this.x, this.y, this.z, 180, this.scene);
				return;
			case "1100": //╚
				this.buildTile("Street_L", this.x, this.y, this.z, 270, this.scene);
				return;
			case "1100": //╚
				this.buildTile("Street_L", this.x, this.y, this.z, 270, this.scene);
				return;
			case "0111": //╩
				//this.buildTile("Street_L", this.x, this.y, this.z, 270, this.scene);
				return;
			case "1101": //╦
				//this.buildTile("Street_L", this.x, this.y, this.z, 270, this.scene);
				return;
			default: console.log("Code " + code + " Not exist"); return;
		}
	}
}