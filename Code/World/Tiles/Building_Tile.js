class Building_Tile extends Tile{
	constructor(x,y,z,rotation,scene, grid, gridSize){
		super(x,y,z,rotation,scene, grid, gridSize);
		this.occupied = false;
		this.building = undefined;
	}
}