var size = 10;
var offset = 6;
var nextHashCode = 0;

const directions = {
			UP: 0,
			RIGHT: 1,
			DOWN: 2,
			LEFT: 3
		}

class Tile{
	constructor(x,y,z,rotation,threeScene, grid, gridsize){
		this.x = x;
		this.y = y;
		this.z = z;
		this.gridSize = gridsize;
		this.rotation = rotation;
		this.mesh;
		this.scene = threeScene;
		this.grid = grid;

		this.hashCode = nextHashCode;
		nextHashCode++;

		this.neighbors = [];
	}

	getNeighbor(dir){
		return this.neighbors[dir];
	}

	updateNeighbors(x,z){

		if(z+1 < this.gridSize){
			this.neighbors.push(this.grid[x][z+1]);
		}else{
			this.neighbors.push(undefined);
		}

		if(x+1 < this.gridSize){
			this.neighbors.push(this.grid[x+1][z]);
		}else{
			this.neighbors.push(undefined);
		}

		if(z-1 >= 0){
			this.neighbors.push(this.grid[x][z-1]);
		}else{
			this.neighbors.push(undefined);
		}

		if(x-1 >= 0){
			this.neighbors.push(this.grid[x-1][z]);
		}else{
			this.neighbors.push(undefined);
		}
	}

	render(){
		if(this.mesh != undefined){
			this.scene.add(this.mesh);
		}
	}

	stopRender(){
		this.scene.remove(this.mesh);
	}

	buildTile(url,x,y,z,rotation,scene){

		this.mesh = getResource(url);
		
		this.mesh.scale.set(size,size,size);

		this.mesh.position.set(x*(size+offset) ,y,z*(size+offset));	
		this.mesh.rotation.set(0, (Math.PI/180) * -rotation, 0);
	}

	buildTileWithMesh(mesh,x,y,z,rotation,scene){

		this.mesh = mesh;

		this.mesh.scale.set(size,size,size);
		this.mesh.position.set(x*(size+offset) ,y,z*(size+offset));	
		this.mesh.rotation.set(0, (Math.PI/180) * -rotation, 0);
	}
}