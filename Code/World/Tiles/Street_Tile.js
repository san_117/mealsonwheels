﻿class Street_Tile extends Tile{
	constructor(x,y,z,rotation,scene, grid, gridSize){
		super(x,y,z,rotation,scene, grid, gridSize);
	}

	checkDirection(direction){
		switch(direction){
			case directions.UP:
				return (this.neighbors[directions.UP] != undefined && this.neighbors[directions.UP].constructor == Street_Tile);
			case directions.LEFT:
				return (this.neighbors[directions.LEFT] != undefined && this.neighbors[directions.LEFT].constructor == Street_Tile);
			case directions.RIGHT:
				return (this.neighbors[directions.RIGHT] != undefined && this.neighbors[directions.RIGHT].constructor == Street_Tile);
			case directions.DOWN:
				return (this.neighbors[directions.DOWN] != undefined && this.neighbors[directions.DOWN].constructor == Street_Tile);
		}
	}

	getValidDirections(direction){
		var validDirections = [];

		switch(direction){
			case directions.UP:
				if(this.neighbors[directions.LEFT] != undefined && this.neighbors[directions.LEFT].constructor == Street_Tile) validDirections.push(directions.LEFT);
				if(this.neighbors[directions.UP] != undefined && this.neighbors[directions.UP].constructor == Street_Tile) validDirections.push(directions.UP);
				if(this.neighbors[directions.RIGHT] != undefined && this.neighbors[directions.RIGHT].constructor == Street_Tile) validDirections.push(directions.RIGHT);
				break;
			case directions.LEFT:
				if(this.neighbors[directions.UP] != undefined && this.neighbors[directions.UP].constructor == Street_Tile) validDirections.push(directions.UP);
				if(this.neighbors[directions.DOWN] != undefined && this.neighbors[directions.DOWN].constructor == Street_Tile) validDirections.push(directions.DOWN);
				if(this.neighbors[directions.LEFT] != undefined && this.neighbors[directions.LEFT].constructor == Street_Tile) validDirections.push(directions.LEFT);
				break;
			case directions.RIGHT:
				if(this.neighbors[directions.RIGHT] != undefined && this.neighbors[directions.RIGHT].constructor == Street_Tile) validDirections.push(directions.RIGHT);
				if(this.neighbors[directions.DOWN] != undefined && this.neighbors[directions.DOWN].constructor == Street_Tile) validDirections.push(directions.DOWN);
				if(this.neighbors[directions.UP] != undefined && this.neighbors[directions.UP].constructor == Street_Tile)validDirections.push(directions.UP);
				break;
			case directions.DOWN:
				if(this.neighbors[directions.LEFT] != undefined && this.neighbors[directions.LEFT].constructor == Street_Tile) validDirections.push(directions.LEFT);
				if(this.neighbors[directions.DOWN] != undefined && this.neighbors[directions.DOWN].constructor == Street_Tile) validDirections.push(directions.DOWN);
				if(this.neighbors[directions.RIGHT] != undefined && this.neighbors[directions.RIGHT].constructor == Street_Tile) validDirections.push(directions.RIGHT);
				break;
		}
		return validDirections;
	}

	buildRoad(){
		this.updateNeighbors(this.x, this.z);
		
		var code = "";

		if(this.neighbors[directions.UP] != undefined && this.neighbors[directions.UP].constructor == Street_Tile)
			code += "1";
		else
			code += "0";

		if(this.neighbors[directions.LEFT] != undefined && this.neighbors[directions.LEFT].constructor == Street_Tile)
			code += "1";
		else
			code += "0";

		if(this.neighbors[directions.DOWN] != undefined && this.neighbors[directions.DOWN].constructor == Street_Tile)
			code += "1";
		else
			code += "0";

		if(this.neighbors[directions.RIGHT] != undefined && this.neighbors[directions.RIGHT].constructor == Street_Tile)
			code += "1";
		else
			code += "0";

		switch(code){
			case "1010": //║
				this.buildTile("Street_I", this.x, this.y, this.z, 0, this.scene);
				return;
			case "0101": //═
				this.buildTile("Street_I", this.x, this.y, this.z, 90, this.scene);
				return;
			case "0110": //╔
				this.buildTile("Street_L", this.x, this.y, this.z, 180, this.scene);
				return;
			case "0011": //╗
				this.buildTile("Street_L", this.x, this.y, this.z, 270, this.scene);
				return;
			case "1001": //╝
				this.buildTile("Street_L", this.x, this.y, this.z, 0, this.scene);
				return;
			case "1100": //╚
				this.buildTile("Street_L", this.x, this.y, this.z, 90, this.scene);
				return;
			case "1101": //╦
				this.buildTile("Street_T", this.x, this.y, this.z, 0, this.scene);
				return;
			case "1110": //╠
				this.buildTile("Street_T", this.x, this.y, this.z, 90, this.scene);
				return;
			case "0111": //╩
				this.buildTile("Street_T", this.x, this.y, this.z, 180, this.scene);
				return;
			case "1011": //╣
				this.buildTile("Street_T", this.x, this.y, this.z, 270, this.scene);
				return;
			case "1111": //╬
				this.buildTile("Street_X", this.x, this.y, this.z, 0, this.scene);
				return;
			
			default: console.log("Code " + code + " Not exist"); return;
		}
	}
}