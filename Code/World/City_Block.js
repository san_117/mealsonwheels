const themes = {
			ARAB: 0,
			ITALIAN: 1,
			JAPAN: 2,
			URBAN: 3,
			MEXICAN: 4
		}
		
class City_Block{
	constructor(tiles){
		this.tiles = tiles;
		this.center = this.calculateCenter();
		this.theme = themes.URBAN;
		this.buildings = [];
	}

	calculateCenter(){
		var averageX = 0;
		var averageZ = 0;
		for(var i = 0; i < this.tiles.length; i++){
			averageX += this.tiles[i].x;
			averageZ += this.tiles[i].z;
		}

		var centerX = Math.floor(averageX / this.tiles.length);
		var centerZ = Math.floor(averageZ / this.tiles.length);

		return new THREE.Vector3(centerX,0, centerZ)
	}

	setTheme(theme){
		this.theme = theme;
		this.calculateBuildings();
	}

	calculateBuildings(){

		var center_tiles = [];
		var border_tiles = [];

		for(var i = 0; i < this.tiles.length;i++){
			var roadCount = 0;

			for(var e = -1; e < 2; e++){
				for(var f = -1; f < 2; f++){
					var x = this.tiles[i].x + e;
					var z = this.tiles[i].z + f;

					if(this.tiles[i].grid[x][z] instanceof Street_Tile){
						roadCount++;
					}
				}
			}

			if(roadCount != 0){
				border_tiles.push(this.tiles[i]);
			}else{
				center_tiles.push(this.tiles[i]);
			}
		}

		var aviableTiles = border_tiles.length;

		var medium = 0;

		//Calculate how many buildings
		while(aviableTiles > 0){
			if(this.theme == themes.MEXICAN){
				if(Math.random() > 0.7){
					aviableTiles -= 2;
					medium += 2;
					continue;
				}
			}else{
				if(Math.random() > 0.3){
					aviableTiles -= 2;
					medium += 2;
					continue;
				}
			}
			aviableTiles--;
		}

		var type = 0; //Food

		if(Math.random() > 0.1){
			type = 1; //House
		}

		var occuped = [];

		for(var i = 0; i < medium; i++){
			var pivot = border_tiles[Math.floor(Math.random() * (border_tiles.length-1))];
			var origin = pivot;

			//2 = ++
			//3 = -+
			//0 = --
			//1 = +-
			var quadrant = 0;

			if(origin.x >= this.center.x){
				if(origin.z >= this.center.z){
					quadrant = 2
				}else{
					quadrant = 1;
				}
			}else{
				if(origin.z >= this.center.z){
					quadrant = 3;
				}else{
					quadrant = 0;
				}
			}

			var zone = [];
			switch(quadrant){
				//xo
				//xx
				case 0:
					zone.push(pivot);
					zone.push(pivot.grid[pivot.x+1][pivot.z]);
					zone.push(pivot.grid[pivot.x+1][pivot.z+1]);
					zone.push(pivot.grid[pivot.x][pivot.z+1]);
					pivot = pivot.grid[pivot.x][pivot.z+1];
				break;
				//ox
				//xx
				case 1:
					zone.push(pivot);
					zone.push(pivot.grid[pivot.x][pivot.z+1]);
					zone.push(pivot.grid[pivot.x-1][pivot.z+1]);
					zone.push(pivot.grid[pivot.x-1][pivot.z]);
					pivot = pivot.grid[pivot.x-1][pivot.z+1];
				break;
				//xx
				//ox
				case 2:
					zone.push(pivot);
					zone.push(pivot.grid[pivot.x][pivot.z-1]);
					zone.push(pivot.grid[pivot.x-1][pivot.z]);
					zone.push(pivot.grid[pivot.x-1][pivot.z-1]);
					pivot = pivot.grid[pivot.x-1][pivot.z];
				break;
				//xx
				//xo
				case 3:
					zone.push(pivot);
					zone.push(pivot.grid[pivot.x+1][pivot.z]);
					zone.push(pivot.grid[pivot.x][pivot.z-1]);
					zone.push(pivot.grid[pivot.x+1][pivot.z-1]);
				break;
			}

			var canBuild = true;
			for(var e = 0; e < zone.length; e++){
				if(occuped.includes(zone[e].hashCode)){
					canBuild = false;
					break;
				}
			}

			if(canBuild){
				for(var e = 0; e < zone.length; e++){
					occuped.push(zone[e].hashCode);
					zone[e].occupied = true;
				}

				var building = new Building(pivot, 2, this.theme, 1);
				pivot.building = building;
				this.buildings.push(building);
			}
		}

		for(var e = 0 ; e < border_tiles.length; e++){

			if(!border_tiles[e].occupied){
				var building = new Building(border_tiles[e], 1, this.theme, type);
				pivot.building = building;
				this.buildings.push(building);
				border_tiles[e].occupied = true;
			}
		}

		for(var e = 0 ; e < center_tiles.length; e++){

			if(!center_tiles[e].occupied){
				var building = new Building(center_tiles[e], 1, this.theme, 2);
				pivot.building = building;
				this.buildings.push(building); //Decos
				center_tiles[e].occupied = true;
			}
		}

		this.spawnBuildings();
	}

	spawnBuildings(){
		for(var i = 0; i < this.buildings.length; i++){
			this.buildings[i].build();
		}
	}

	debugTheme(){
		for(var i = 0; i < this.tiles.length;i++){
			this.tiles[i].buildTile("Debug",this.tiles[i].x,0,this.tiles[i].z,this.tiles[i].rotation,this.tiles[i].scene);
			
			var aux = new THREE.MeshStandardMaterial();
			aux.map = this.tiles[i].mesh.material.map;

			switch(this.theme){
				case themes.ARAB:	
					aux.color = new THREE.Color( 'black' );
					this.tiles[i].mesh.material = aux;
					break;
				case themes.ITALIAN:	
					aux.color = new THREE.Color( 'green' );
					this.tiles[i].mesh.material = aux;
					break;
				case themes.JAPAN:	
					aux.color = new THREE.Color( 'red' );
					this.tiles[i].mesh.material = aux;
					break;
				case themes.URBAN:	
					aux.color = new THREE.Color( 'blue' );
					this.tiles[i].mesh.material = aux;
					break;
				case themes.MEXICAN:
					aux.color = new THREE.Color( 'yellow' );
					this.tiles[i].mesh.material = aux;
					break;
			}
			//console.log(this.tiles[i].mesh.material.color);
		}
	}

	debugCenter(){
		for(var i = 0; i < this.tiles.length; i++){
			if(this.tiles[i].x == this.center.x && this.tiles[i].z == this.center.z){
				this.tiles[i].buildTile("Debug",this.center.x,0,this.center.z,this.tiles[i].rotation,this.tiles[i].scene);
			}
		}
	}
}