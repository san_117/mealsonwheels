class Building{
	constructor(tile, size, theme, type){
		this.pivot = tile;
		this.size = size;
		this.theme = theme;
		this.type = type;
	}
	//Type
	//0 = Food
	//1 = House
	//2 = Deco

	build(){
		if(this.theme == themes.ARAB){
			var prob_house			= Math.floor(Math.random() * (arab_buildings_houses.length));
			var prob_house_small	= Math.floor(Math.random() * (arab_buildings_houses_small.length));
			var prob_food			= Math.floor(Math.random() * (arab_buildings_food.length));
			var prob_deco			= Math.floor(Math.random() * (arab_buildings_deco.length));

			if(this.type == 2){
				this.pivot.buildTileWithMesh(arab_buildings_deco[prob_deco].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
				this.pivot.building = this;
				return;
			}else if (this.type == 1){
				if(this.size == 2){
					this.pivot.buildTileWithMesh(arab_buildings_houses[prob_house].clone(),this.pivot.x,0,this.pivot.z,this.pivot.rotation,this.pivot.scene);
					this.pivot.building = this;
					return;
				}else{
					this.pivot.buildTileWithMesh(arab_buildings_houses_small[prob_house_small].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
					this.pivot.building = this;
					return;
				}
			}else{
				this.pivot.buildTileWithMesh(arab_buildings_food[prob_food].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
				this.pivot.building = this;
				return;
			}
		}

		if(this.theme == themes.URBAN){
			var prob_house			= Math.floor(Math.random() * (urban_buildings_houses.length));
			var prob_house_small	= Math.floor(Math.random() * (urban_buildings_houses_small.length));
			var prob_food			= Math.floor(Math.random() * (urban_buildings_food.length));
			var prob_deco			= Math.floor(Math.random() * (urban_buildings_deco.length));

			if(Math.random()>0.3)
				prob_deco = 0;

			if(this.type == 2){
				this.pivot.buildTileWithMesh(urban_buildings_deco[prob_deco].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
				this.pivot.building = this;
				return;
			}else if (this.type == 1){
				if(this.size == 2){
					this.pivot.buildTileWithMesh(urban_buildings_houses[prob_house].clone(),this.pivot.x,0,this.pivot.z,this.pivot.rotation,this.pivot.scene);
					this.pivot.building = this;
					return;
				}else{
					this.pivot.buildTileWithMesh(urban_buildings_houses_small[prob_house_small].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
					this.pivot.building = this;
					return;
				}
			}else{
				this.pivot.buildTileWithMesh(urban_buildings_food[prob_food].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
				this.pivot.building = this;
				return;
			}
		}

		if(this.theme == themes.JAPAN){
			var prob_house			= Math.floor(Math.random() * (japan_buildings_houses.length));
			var prob_house_small	= Math.floor(Math.random() * (japan_buildings_houses_small.length));
			var prob_food			= Math.floor(Math.random() * (japan_buildings_food.length));
			var prob_deco			= Math.floor(Math.random() * (japan_buildings_deco.length));

			if(this.type == 2){
				this.pivot.buildTileWithMesh(japan_buildings_deco[prob_deco].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
				this.pivot.building = this;
				return;
			}else if (this.type == 1){
				if(this.size == 2){
					this.pivot.buildTileWithMesh(japan_buildings_houses[prob_house].clone(),this.pivot.x,0,this.pivot.z,this.pivot.rotation,this.pivot.scene);
					this.pivot.building = this;
					return;
				}else{
					this.pivot.buildTileWithMesh(japan_buildings_houses_small[prob_house_small].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
					this.pivot.building = this;
					return;
				}
			}else{
				this.pivot.buildTileWithMesh(japan_buildings_food[prob_food].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
				this.pivot.building = this;
				return;
			}
		}

		if(this.theme == themes.MEXICAN){
			var prob_house			= Math.floor(Math.random() * (mexican_buildings_houses.length));
			var prob_house_small	= Math.floor(Math.random() * (mexican_buildings_houses_small.length));
			var prob_food			= Math.floor(Math.random() * (mexican_buildings_food.length));
			var prob_deco			= Math.floor(Math.random() * (mexican_buildings_deco.length));

			if(Math.random()>0.3)
				prob_house = 0;

			if(this.type == 2){
				this.pivot.buildTileWithMesh(mexican_buildings_deco[prob_deco].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
				this.pivot.building = this;
				return;
			}else if (this.type == 1){
				if(this.size == 2){
					this.pivot.buildTileWithMesh(mexican_buildings_houses[prob_house].clone(),this.pivot.x,0,this.pivot.z,this.pivot.rotation,this.pivot.scene);
					this.pivot.building = this;
					return;
				}else{
					this.pivot.buildTileWithMesh(mexican_buildings_houses_small[prob_house_small].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
					this.pivot.building = this;
					return;
				}
			}else{
				this.pivot.buildTileWithMesh(mexican_buildings_food[prob_food].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
				this.pivot.building = this;
				return;
			}
		}

		if(this.theme == themes.ITALIAN){
			var prob_house			= Math.floor(Math.random() * (italian_buildings_houses.length));
			var prob_house_small	= Math.floor(Math.random() * (italian_buildings_houses_small.length));
			var prob_food			= Math.floor(Math.random() * (italian_buildings_food.length));
			var prob_deco			= Math.floor(Math.random() * (italian_buildings_deco.length));

			if(this.type == 2){
				this.pivot.buildTileWithMesh(italian_buildings_deco[prob_deco].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
				this.pivot.building = this;
				return;
			}else if (this.type == 1){
				if(this.size == 2){
					this.pivot.buildTileWithMesh(italian_buildings_houses[prob_house].clone(),this.pivot.x,0,this.pivot.z,this.pivot.rotation,this.pivot.scene);
					this.pivot.building = this;
					return;
				}else{
					this.pivot.buildTileWithMesh(italian_buildings_houses_small[prob_house_small].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
					this.pivot.building = this;
					return;
				}
			}else{
				this.pivot.buildTileWithMesh(italian_buildings_food[prob_food].clone(),this.pivot.x,0,this.pivot.z,Math.floor(Math.random()*5)*90,this.pivot.scene);
				this.pivot.building = this;
				return;
			}
		}
	}
}