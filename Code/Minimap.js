class Minimap extends Scene
{
    constructor(engine, world)
    {
        var mapCamera = new THREE.OrthographicCamera(
            -800,		// Left
            800,		// Right
            400,		// Top
            -400,		// Bottom
            1,            			// Near 
            1000 );           			// Far 
    
        mapCamera.up = new THREE.Vector3(0,0,-1);
	    mapCamera.lookAt( new THREE.Vector3(0,-1,0) );
	    mapCamera.position.y = 500;
		//mapCamera.zoom = 2;
		mapCamera.updateProjectionMatrix();
        super(engine, mapCamera);
        this.camera = mapCamera;
        this.scene= world;
    }   
}