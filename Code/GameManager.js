var lives = 3;

class GameManager extends GameObject{
	constructor(world, player, hud){
		super();
		this.cooldown = 100;
		this.goalTime = 10;
		this.world = world;
		this.score = 0;
		this.orders = [];
		this.building_tiles = [];
		this.phone = hud.hud.phone;
		this.hud = hud.hud;
		this.player = player.player;
		this.gameover = false;
		lives = 3;
	}

	start(){
		//this.resetCooldown();	

		for(var x = 0; x < this.world.size; x++){
			for(var y = 0; y < this.world.size; y++){
				if(this.world.grid[x][y].constructor == Building_Tile){
					if(this.world.grid[x][y].building != undefined){
						if(this.world.grid[x][y].building.type != 2){
							this.building_tiles.push(this.world.grid[x][y]);
						}
					}
				}
			}
		}
	}
	
	update(){	
		if(lives == 0)
		{
			deltaTime = 0;

			if(!this.gameover){
				this.hud.gameOver();
				this.gameover = true;
			}

			return;
		}

		if(this.cooldown >= this.goalTime){
			console.log(this.orders.length);
			if(this.orders.length < 3){
				var order = new GameOrder(this.building_tiles, this.world);
				this.orders.push(order);
				this.phone.createOrder(order.food.building.theme, order.house.building.theme,order);
			}

			this.resetCooldown();
		}else{
			this.cooldown += deltaTime;
		}

		this.paintMarkers();
	}

	paintMarkers(){
		for(var i = 0; i < this.orders.length; i++){
			if(this.orders[i].marker == undefined){
				this.orders[i].marker = getResource("Arrow").clone();
		
				this.orders[i].marker.scale.set(10,10,10);

				this.orders[i].marker.position.set(this.orders[i].target.x*(10+6) ,15,this.orders[i].target.z*(10+6));	
				this.world.scene.scene.add(this.orders[i].marker);
			}

			this.orders[i].marker.position.set(this.orders[i].target.x*(10+6) ,15,this.orders[i].target.z*(10+6));
			this.orders[i].marker.rotation.set(0,this.orders[i].marker.rotation.y + ((Math.PI/180) * (deltaTime*30)),0);
			
			if(this.orders[i].status == -1){
				this.orders.splice(i,1);
				return;
			}

			if(this.player.x == this.orders[i].target.x && this.player.z == this.orders[i].target.z){
				if(this.orders[i].status == 0){
					this.orders[i].completeFood();
				}else if (this.orders[i].status == 1){
					this.orders[i].completeOrder();
					this.score += 10;
					this.hud.updateScore(this.score);
					this.world.scene.scene.remove(this.orders[i].marker);
					this.orders.splice(i,1);
					return;
				}
			}	
		}
	}

	resetCooldown(){
		this.cooldown = 0;
	}
}

class GameOrder{
	constructor(building_tiles, world){
		this.house = this.getBuilding(building_tiles,1);
		this.house_street = this.findStreet(this.house,world);
		console.log(this.house_street);
		this.food= this.getBuilding(building_tiles,0);
		this.food_street = this.findStreet(this.food,world);
		this.world = world;

		this.target = new THREE.Vector3(this.food_street.x,0,this.food_street.z);

		//Status
		//-1 = Fail
		//1 = In Restaorant
		//2 = In House
		this.status = 0;
		this.marker = undefined;
	}

	completeFood(){
		this.status = 1;
		this.target = new THREE.Vector3(this.house_street.x,0,this.house_street.z);
	}

	completeOrder(){
		this.status = 2;
		this.target = new THREE.Vector3(this.house_street.x,5000,this.house_street.z);
	}

	failOrder(){
		lives--;
		this.status = -1;
		this.target = new THREE.Vector3(0,5000,0);
		this.world.scene.scene.remove(this.marker);
	}

	findStreet(tile, world){
		if(world.grid[tile.x+1][tile.z].constructor == Street_Tile){
			return world.grid[tile.x+1][tile.z];
		}

		if(world.grid[tile.x-1][tile.z].constructor == Street_Tile){
			return world.grid[tile.x-1][tile.z];
		}

		if(world.size && world.grid[tile.x][tile.z+1].constructor == Street_Tile){
			return world.grid[tile.x][tile.z+1];
		}

		if(world.grid[tile.x][tile.z-1].constructor == Street_Tile){
			return world.grid[tile.x][tile.z-1];
		}
	}

	getBuilding(building_tiles, type, world){
	
		while(true){
			
			var i = Math.floor(Math.random()*(building_tiles.length));
			
			var tile = building_tiles[i];
			if(tile.building.type == type){
				return tile;
			}
		}
	}
}